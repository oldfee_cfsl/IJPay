# IJPay

![](assets/img/IJPay-t.png)

[![Gitee star](https://gitee.com/javen205/IJPay/badge/star.svg?theme=white)](https://gitee.com/javen205/IJPay/stargazers)
[![Github start](https://img.shields.io/github/stars/Javen205/IJPay.svg?style=social&label=Stars)](https://github.com/Javen205/IJPay)
[![License][licensesvg]][license]
[![JDK](https://img.shields.io/badge/JDK-1.6+-green.svg)](https://www.oracle.com/technetwork/java/javase/downloads/index.html)
[![IJPay Author](https://img.shields.io/badge/IJPay%20Author-Javen-ff69b4.svg)](https://javen205.github.io)
[![GitHub release](https://img.shields.io/github/release/Javen205/IJPay.svg)](https://github.com/Javen205/IJPay/release)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.github.javen205/IJPay/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.github.javen205/IJPay)
[![Donate](https://img.shields.io/badge/Donate-WeChat-%23ff3f59.svg)](https://github.com/Javen205/donate)
[![Gitter](https://badges.gitter.im/IJPay/community.svg)](https://gitter.im/IJPay/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

**特别说明：** 不依赖任何第三方 mvc 框架，仅仅作为工具使用简单快速完成支付模块的开发，可轻松嵌入到任何系统里。


Gitee：[http://gitee.com/Javen205/IJPay](http://gitee.com/Javen205/IJPay)

GitHub：[https://github.com/Javen205/IJPay](https://github.com/Javen205/IJPay)

APIDOC：[https://apidoc.gitee.com/javen205/IJPay](https://apidoc.gitee.com/javen205/IJPay)



- IJPay是[JPay](https://github.com/Javen205/JPay)后台接口SDK不依赖任何第三方 mvc 框架，仅仅作为工具使用简单快速完成支付模块的开发，可轻松嵌入到任何系统里。
- [JPay](https://github.com/Javen205/JPay) 是简易而不简单的Android 支付SDK，JPay是对微信App支付、支付宝App支付的二次封装,对外提供一个相对简单的接口以及支付结果的回调。
- IOS支付SDK，敬请关注。

### 接入文档


[IJPay 让支付触手可及](https://javen205.gitee.io/ijpay/)


### 效果图

**请参考Demo，目前IJPay Demo 提供两个版本 JFinal 版本以及 Spring Boot 版本**

Gitee：[https://gitee.com/Javen205/IJPay-Demo](https://gitee.com/Javen205/IJPay-Demo)

GitHub：[https://github.com/Javen205/IJPay-Demo](https://github.com/Javen205/IJPay-Demo)


### 联系方式

[![](https://img.shields.io/badge/IJPay%20%E4%BA%A4%E6%B5%81%E7%BE%A4-723992875-fba7f9.svg)](http://shang.qq.com/wpa/qunwpa?idkey=44c2b0331f1bdca6c9d404e863edd83973fa97224b79778db79505fc592f00bc)
[![Email](https://img.shields.io/badge/Email-javendev%40126.com-yellowgreen.svg)](http://javen.blog.csdn.net)



[licensesvg]: https://img.shields.io/badge/License-Apache--2.0-brightgreen.svg
[license]: https://www.apache.org/licenses/LICENSE-2.0




### 贡献代码

最后如果该库对你有帮助不妨右上角点点Star或者任意打赏支持一下，我更喜欢你 Fork PR 成为项目贡献者 .

[前往捐赠](https://github.com/Javen205/donate)


### 鸣谢

排名不分先后

- [huTool](https://hutool.cn) huTool 是一个Java工具包
- [alipay-sdk-java](https://github.com/alipay/alipay-sdk-java-all)  蚂蚁金服开放平台 Java SDK 


## 开源推荐

- `TNW` 微信公众号开发脚手架：https://gitee.com/javen205/TNW
- SpringBoot 微服务高效开发 `mica` 工具集：https://gitee.com/596392912/mica
- `Avue` 一款基于 vue 可配置化的神奇框架：https://gitee.com/smallweigit/avue
- `pig` 宇宙最强微服务（架构师必备）：https://gitee.com/log4j/pig
- `SpringBlade` 完整的线上解决方案（企业开发必备）：https://gitee.com/smallc/SpringBlade


## 官方文档 

- [微信委托扣款 API](https://pay.weixin.qq.com/wiki/doc/api/pap_sl.php?chapter=17_1)
- [微信刷脸支付 API](https://pay.weixin.qq.com/wiki/doc/wxfacepay/develop/backend.html)
- [支付宝开发平台](https://docs.open.alipay.com)